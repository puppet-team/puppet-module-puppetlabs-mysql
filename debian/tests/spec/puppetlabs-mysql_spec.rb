require 'spec_helper'

describe "mariadb is not installed" do
  describe package('mariadb') do
    it { should_not be_installed }
  end
  describe service('mariadb') do
    it { should_not be_running }
  end
end

describe "mysql::client class applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'include mysql::client'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "mariadb-client is installed" do
  describe package('mariadb-client') do
    it { should be_installed }
  end
end

describe "mysql::server class applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'include mysql::server'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "mariadb-server is installed" do
  describe package('mariadb-server') do
    it { should be_installed }
  end
end

describe "mariadb service is started" do
  # allow time for the service to start
  before(:all) do
    sleep 5
  end
  describe service('mariadb') do
    it { should be_enabled }
    it { should be_running }
  end
end

describe "mariadb is responsive" do
  describe command("echo '\\s' | mysql") do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match (/Server:\s+MariaDB/) }
  end
end
